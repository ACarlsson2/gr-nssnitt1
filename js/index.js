/*
*   Global variables
*/
var user = sessionStorage.getItem("userName");
var selectedLanguage = sessionStorage.getItem("language");
$("#user-text").html(user);
var beerArray = JSON.parse(sessionStorage.getItem("beerArray"));
var cartArray = [];
var undoArray = [];
var redoArray = [];
var ifCLEAR = false;
var removedBeer = false;
var credits = sessionStorage.getItem("credits");

/*
*   The translation dictionary.
*   To be used as key-value
*/
var translation = { //store your translations in a object.
    "en": {
        "order": "Order",
        "about": "About",
        "inventory": "Inventory",
        "signOut": "Sign Out",
        "allBeer": "All the Drinks",
        "yourOrder": "Your order",
        "clear": "Clear Order",
        "undo": "Undo",
        "redo": "Redo",
        "purchase": "Purchase",
        "dropBeer": "Drop beer here",
        "search": "Search drink",
    },
    "sv": {
        "order": "Beställ",
        "about": "Om oss",
        "inventory": "Lager",
        "signOut": "Logga ut",
        "allBeer": "All dricka",
        "yourOrder": "Din beställning",
        "clear": "Nollställ beställning",
        "undo": "Ångra",
        "redo": "Gör om",
        "purchase": "Köp",
        "dropBeer": "Släpp dricka här",
        "search": "Sök dricka",

    }
}

/*
*   function: language(language)
*   input: laguage to translate to
*   use: translate the page to the desired language by changing the HTML
*/
function translate(language) {
    selectedLanguage = language;
    sessionStorage.setItem("language", selectedLanguage);
  $(".multi-lang").each(function() {
      var tkey = $(this).attr("tkey");
    $(this).html(translation[selectedLanguage][tkey])
  })
};


/*
*   function: ---
*   use: on page load, load the beers and translate the page
*/
$(function() {
    loadBeer();
    translate(selectedLanguage);
});

/*
*   function: loadBeer()
*   input: none
*   use: sends a request to the API for information on all the drinks
*/
function loadBeer(){
    console.log("user = " + user);

    beerArray.forEach(function(ele) {
        var beerContainer = $("#beerContainer");
        var beer = $('<div class="beer" id="'+ele.beer.id+'" draggable="true" ondragstart="drag(event)" ></div>');
        beerContainer.append(beer);
        //console.log(ele.beer.id);


        beer.append('<div class="col-lg-8"><div class="beerName">'+ele.beer.name+'</div></div>');
        beer.append('<div class="col-lg-3"><div class="beerPrice">' + ele.beer.price+ "kr" + '</div></div>');
        beer.append('<div class="col-lg-8"><div class="beerName2">' + ele.beer.name2 + '</div></div>');
        beer.append('<div class="col-lg-3"><div class="beerCount">' + ele.beer.count + '</div></div>');


    });
    checkInv();
}

/*
*   function: checkInv()
*   input: none
*   use: checks the inventory if a drink is in stock. If not a cross is drawn over the drink.
*/
function checkInv() {
    $(".beer").each(function() {
      //console.log($(this).find(".beerCount").text());
      if ($(this).find(".beerCount").text() < 1) {
          //console.log($(this).find(".beerCount").text());
          $(this).prepend('<img class="sold-out" src="res/sold-out2.png" />');
      }
    })
}

/*
*   function: confirmOrder()
*   input: none
*   use: sends a purchase to the API and draws credits from the user.
*/
function confirmOrder() {
    if (credits >= parseInt($("#order-total").text().slice(0,-2))) {
        cartArray.forEach(function(ele) {
        // buy beer API request...
        for (var i = 0; i < ele.amount; i++) {
            $.getJSON( "http://pub.jamaica-inn.net/fpdb/api.php?username="+user+"&password="+user+"&action=purchases_append&beer_id="+ele.id, function(json) {
                    console.log("Beer bought");
                });
            }
            console.log(ele.id);
            updateBeerArray(ele.id, -ele.amount);
        });
        credits = credits - parseInt($("#order-total").text().slice(0,-2));
        $(".left").html("Current credits:" + credits);
        //console.log("Beers bought!");
        cartArray = [];
        updateCartView();
        saveBeer();
    }
    else {

    }

}

/*
*   function: clearOrder()
*   input: none
*   use: clears the cart of drinks and returns them to the inventory
*/
function clearOrder(){
        undoArray.push(cartArray.slice());
        $("#cart-list").empty();
        $("#order-total").html('<h1>0kr</h1>');
        cartArray.forEach(function(ele) {
            console.log("cleared");
            console.log(ele);
            $("#"+ele.id).find(".beerCount").html(parseInt($("#"+ele.id).find(".beerCount").text()) + ele.amount);

        });
        cartArray = [];
        ifCLEAR = true;
        $("#beerContainer").empty();
        loadBeer();
}

/*
*   function: addToCart(id, price)
*   input: id - id of the beer. price - the price of the beer
*   use: adds information of a beer to the cart
*/
function addToCart(id, price) {
    redoArray = [];
    //undoArray.push(JSON.parse(JSON.stringify(cartArray))); // copy array
    cartArray.push({"id": id, "price": price, "amount": 1});
    updateCartView();
}

/*
*   function: updateCart(id)
*   input: id - id of a drink
*   use: update the amount of a drink in the cart
*/
function updateCart(id) {
    //undoArray.push(JSON.parse(JSON.stringify(cartArray)));
    cartArray.forEach(function(ele) {
        if (ele.id == id) {
            ele.amount += 1;
            //console.log(ele.amount);
        }
    });

}

/*
*   function: minusBeer(ev)
*   input: ev - event
*   use: reduce the amount of a drink in the cart by one
*/
function minusBeer(ev) {
    undoArray.push(JSON.parse(JSON.stringify(cartArray)));
    var item = ev.target.closest(".cart-item");
    var id = $(item).find("#beerName").attr("beerid");
    var $menu_beer = $("#"+id);

    for (var i = 0; i < cartArray.length; i++) {
        if (cartArray[i].id == id) {
            if (cartArray[i].amount <= 1) {
                cartArray.splice(i,1);
            }
            else {
                cartArray[i].amount -= 1;
            }

        }
    }
    $menu_beer.find(".beerCount").html(parseInt($menu_beer.find(".beerCount").text()) +1);
    console.log(cartArray);
    updateCartView();
}

/*
*   function: plusBeer(ev)
*   input: ev - event
*   use: adds a drink to the cart by adding one to the amount
*/
function plusBeer(ev) {
    undoArray.push(JSON.parse(JSON.stringify(cartArray)));
    redoArray = [];
    var item = ev.target.closest(".cart-item");
    var id = $(item).find("#beerName").attr("beerid");
    var $menu_beer = $("#"+id);
    $menu_beer.find(".beerCount").html($menu_beer.find(".beerCount").text() -1);
    updateCart(id);
    updateCartView();
    checkInv();
}

/*
*   function: removeBeer(ev)
*   input: ev - event
*   use: removes a drink entirely from the cart
*/
function removeBeer(ev) {
    undoArray.push(JSON.parse(JSON.stringify(cartArray)));

    var item = ev.target.closest(".cart-item");
    var id = $(item).find("#beerName").attr("beerid");
    var amount = $(item).find("#amount").text().slice(0,-2);
    $("#"+id).find(".beerCount").html(parseInt($("#"+id).find(".beerCount").text()) + parseInt(amount));

    for (var i = 0; i < cartArray.length; i++) {
        if (cartArray[i].id == id) {
                cartArray.splice(i,1);
        }
    }
    updateCartView();
    removedBeer = true;

}

/*
*   function: updateCartView()
*   input: none
*   use: update the cart graphically from what drinks that is in the cartArray
*/
function updateCartView() {
    var total = 0;
    $("#cart-list").empty();
    cartArray.forEach(function(ele) {
        $.getJSON( "http://pub.jamaica-inn.net/fpdb/api.php?username="+user+"&password="+user+"&action=beer_data_get&beer_id="+ele.id, function(json) {
                $.each(json.payload, function(i, b) {
                    //console.log(b);
                    var name = b.namn;

                    $("#cart-list").append('<li class="list-group-item cart-item"><span class="badge" id="cart-badge">'+ele.amount*ele.price+"kr"+'</span><div id="beerName" style="width:50%" beerID="'+ele.id+'">'+name+" "+b.namn2+'</div><div id="amount">'+ele.amount+"st"+'</div><a class="minusButton" onclick="minusBeer(event)"></a><a class="plusButton" onclick="plusBeer(event)"></a><a onclick=removeBeer(event) class="closeButton"></a></li>');
                    total += ele.amount*ele.price;
                    //console.log(total);
                    $("#order-total").html('<h1>'+total+'kr</h1>');
                })
        });
    });
    $("#order-total").html('<h1>'+total+'kr</h1>');
}

/*
*   function: updateBeerArray(id, amount)
*   input: id - the id of a drink. amount - amount to change.
*   use: change the amount of a drink.
*/
function updateBeerArray(id, amount) {
    for (var i = 0; i < beerArray.length; i++) {
        if (beerArray[i].beer.id == id) {
            beerArray[i].beer.count = parseInt(beerArray[i].beer.count) + parseInt(amount);
        }
    }
}

/*
*   function: allowDrop(ev)
*   input: ev - event
*   use: allow drop on an element
*/
function allowDrop(ev) {
    ev.preventDefault();
}

/*
*   function: drag(ev)
*   input: ev - event
*   use: when an element is draged the ID is bound
*/
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

/*
*   function: drop(ev)
*   input: ev - event
*   use: handle drop of a drink in to the cart. Adding amount in cart and reduce amount in inventory
*/
function drop(ev) {
    ev.preventDefault();
    var id = ev.dataTransfer.getData("text");
    var $menu_beer = $("#"+id);

    // copy cartArray into undoArray. Save state before
    undoArray.push(JSON.parse(JSON.stringify(cartArray)));

    if ($menu_beer.find(".beerCount").text() > 0) {
        var result = $.grep(cartArray, function(n) { return n.id == id});
        if (result.length != 0) {

            console.log("in cart");
            updateCart(id);
            $menu_beer.find(".beerCount").html($menu_beer.find(".beerCount").text() -1);
            updateCartView();

        } else {
            console.log("not in cart");
            var price = $menu_beer.find(".beerPrice").text().slice(0,-2);
            addToCart(id, price);
            $menu_beer.find(".beerCount").html($menu_beer.find(".beerCount").text() -1);
        }
        redoarray = [];
    }
    checkInv();
}




/*
*   function: undoCart()
*   input: none
*   use: handles the undo functionality of the cart and saves the state of the cart. The function depends on 3 different "states".
*   undo on an clearCart, undo on a removed drink or "normal" state undo
*/
function undoCart(){

    if (ifCLEAR) {
        undoClearCart();
    }
    else if (removedBeer) {
        undoRemovedBeer();
    }
    else {
        if (undoArray.length > 0) {
            var prev = undoArray.pop();
            redoArray.push(JSON.parse(JSON.stringify(cartArray)));

            // compare cartArray with undoArray to put beer "back"
            cartArray.forEach(function(ele) {
                //console.log(ele);
                var found = false;
                for (var i = 0; i < prev.length; i++) {
                    if (ele.id == prev[i].id) {
                        found = true;
                        if (ele.amount > prev[i].amount) {
                            // ändra öl
                            console.log("adding: " + ele.id);
                            $("#"+ele.id).find(".beerCount").html(parseInt($("#"+ele.id).find(".beerCount").text()) +1);
                        }
                    }
                }
                if (!found) {
                    // ändra öl
                    console.log("adding: " + ele.id);
                    $("#"+ele.id).find(".beerCount").html(parseInt($("#"+ele.id).find(".beerCount").text()) + 1);
                }
            });

            cartArray = prev;
            updateCartView();

        }
        //console.log(cartArray);
        //console.log(redoArray);
    }

}







/*
*   function: redoCart()
*   input: none
*   use: handles the redo functionality on the cart to redo an action and "go forth" one state
*/

function redoCart(){
    if (redoArray.length > 0) {
        var next = redoArray.pop();
        undoArray.push(JSON.parse(JSON.stringify(cartArray)));

        next.forEach(function(ele) {
            //console.log(ele);
            var found = false;
            for (var i = 0; i < cartArray.length; i++) {
                if (ele.id == cartArray[i].id) {
                    found = true;
                    if (ele.amount > cartArray[i].amount) {
                        // minska på öl med 1
                        console.log("removing: " + ele.id);
                        $("#"+ele.id).find(".beerCount").html(parseInt($("#"+ele.id).find(".beerCount").text()) - 1);
                    }
                }
            }
            if (!found) {
                // minska öl med 1
                console.log("removing: " + ele.id);
                $("#"+ele.id).find(".beerCount").html(parseInt($("#"+ele.id).find(".beerCount").text()) - 1);
            }
        });

        cartArray = next;
        updateCartView();
    }

}





/*
*   function: undoClearCart()
*   input: none
*   use: when the clear cart button is pressed the inventory in the cart is saved to be restored with undo.
*   This function restores a cleard cart.
*/

function undoClearCart(){
    var prev = undoArray.pop();
    cartArray = prev;

    cartArray.forEach(function(ele) {
        $("#"+ele.id).find(".beerCount").html(parseInt($("#"+ele.id).find(".beerCount").text()) - ele.amount);

    });
    updateCartView();
    ifCLEAR = false;
}


/*
*   function: undoRemovedBeer()
*   input: none
*   use: Handles the undo of a drink that has beed removed with the cross button in the cart.
*/
function undoRemovedBeer() {
    var prev = undoArray.pop();

    prev.forEach(function(ele) {
        //console.log(ele);
        var found = false;
        for (var i = 0; i < cartArray.length; i++) {
            if (ele.id == cartArray[i].id) {
                found = true;
            }
        }
        if (!found) {
            // minska öl med 1
            console.log("removing: " + ele.id);
            $("#"+ele.id).find(".beerCount").html(parseInt($("#"+ele.id).find(".beerCount").text()) - ele.amount);
        }
    });

    cartArray = prev;
    updateCartView();
    removedBeer = false;

}

/*
*   adds the users credit to the modal
*/
$(".left").html("Current credits:" + credits);


/*
*   function: addCredits()
*   input: none
*   use: updates the users credit
*/
function addCredits(){
    var addCredit = $("#credit-amount").val();
    credits += parseInt(addCredit);
    $(".left").html("Current credits:" + credits);
    $("#credit-amount").val("");
    $("#credit-amount").focus();
    //console.log(credits);
}


/*
*   function: saveBeer()
*   input: none
*   use: saves the array that stores the different drinks and saves the users credit.
*/
function saveBeer() {
    sessionStorage.setItem("beerArray", JSON.stringify(beerArray));
    sessionStorage.setItem("credits", credits);
}


/*
*   functionallity for the search field.
*   hides and show elements depending on the input
*/
$("#search").keyup(function() {
    //console.log("change");
    $(".beer").each(function() {
        var search = $("#search").val().toLowerCase();
        //console.log($(this).find(".beerName").text());
        if (search != "") {
            if ($(this).find(".beerName").text().toLowerCase().indexOf(search) < 0) {
                //console.log($(this).find(".beerName").text().toLowerCase());
                $(this).hide();
            }
            else {
                $(this).show();
            }
        }
        else $(this).show();


    });
});
