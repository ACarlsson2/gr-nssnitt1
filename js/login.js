/*
*   Global variables,
*   One array of admins and one array of normal users
*/

var admins = ["ervtod", "hirchr", "jorass", "saskru", "svetor"];
var users = ["aamsta", "anddar", "ankov", "aqulyn", "aubbla", "benfau",
"bratam", "ceznew", "dansch", "didwat", "domolh", "edraug",
"einyam", "elepic", "eulcou", "eusgor", "felbar", "felfra", "fercrn",
"giamik", "gollan", "hyrlap", "jacabb", "janhei", "jeaats", "jershi",
"jovsit", "karbly", "katfab", "kaywan", "kenolg", "krysan", "larsch",
"lasnic", "liatra", "livzha", "maihon", "marpug", "marsti", "molbab",
"muhtof", "nikpro", "olislu", "olubra", "oludra", "orapan", "pauaaf",
"pomgra", "prabar", "rewes", "schjou", "shapet", "sivan", "steber",
"sulpen", "sulstr", "symzim", "teojen", "tohei", "valpag", "yevowe", "zulgor"];

var selectedLanguage = "en";
var beerArray = [];

/*
*   The translation dictionary.
*   To be used as key-value
*/
var translation = {
    "en": {
        "signIn": "Sign in"
    },
    "sv": {
        "signIn": "Logga in",

    }
}

/*
*   function: language(lang)
*   input: laguage to translate to
*   use: translate the page to the desired language by changing the HTML
*/
function translate(language) {
    selectedLanguage = language;
  $(".multi-lang").each(function() {
      var tkey = $(this).attr("tkey");
    $(this).html(translation[selectedLanguage][tkey])
  })
};

/*
*   function: loadBeer(user)
*   input: the username of the user that sign in
*   use: sends a request to the API for information on all the drinks
*/
function loadBeer(user){
    console.log("user = " + user);
    $.getJSON( "http://pub.jamaica-inn.net/fpdb/api.php?username="+user+"&password="+user+"&action=inventory_get", function(json) {
            $.each(json.payload, function(i, ele) {
                console.log(ele);
                //var beerContainer = $("#beerContainer");

                if (ele.namn) {
                    //var beer = $('<div class="beer" id="'+ele.beer_id+'" draggable="true" ondragstart="drag(event)" onclick=beerClick(event) ></div>');
                    //beerContainer.append(beer);
                    var beer = {"beer": {
                        "id": ele.beer_id,
                        "name":ele.namn,
                        "name2":ele.namn2,
                        "price": ele.pub_price,
                        "orderPrice": ele.price,
                        "count": ele.count
                    }}

                    beerArray.push(beer);

                }

            })
    });
}

/*
*   Handles the sign in form. Checks if a user is an admin or normal user and redirects to the right page.
*/
$( "#signin" ).submit(function( event ) {
    // check if the user is an admin
  if ( jQuery.inArray( $("#inputUser").val(), admins) != -1 ) {
      if ($("#inputUser").val() === $("#inputPassword").val()) {
          console.log($("#inputUser").val() + "signed in");
          // save the signed in user
          loadBeer($("#inputUser").val());

          setTimeout(
              function() {
                  // save data in sessionStorage
                  sessionStorage.setItem("userName", $("#inputUser").val());
                  sessionStorage.setItem("language", selectedLanguage);
                  sessionStorage.setItem("beerArray", JSON.stringify(beerArray));
                  sessionStorage.setItem("credits", 99999);
                  //console.log(beerArray);
                  window.location.href ="bartender.html";
              }, 500);

          // print welcome message and redirect
          //window.location.href ="bartender.html";

      }
      else {
          // wrong psw for the admin
          console.log("wrong password");
          $("#inputPassword").val('');
          $("#inputPassword").focus();
          $("#wrongPSW").html("<p>Wrong username or password</p>").delay(100).fadeIn(300);
          $("#wrongPSW").delay(800).fadeOut(200);
      }
      // check if the user is a normal user
} else if (jQuery.inArray( $("#inputUser").val(), users) != -1) {
    if ($("#inputUser").val() === $("#inputPassword").val()) {
        console.log($("#inputUser").val() + "signed in");
        // save the signed in user


        loadBeer($("#inputUser").val());


        setTimeout(
            function() {
                // save data in sessionStorage
                sessionStorage.setItem("userName", $("#inputUser").val());
                sessionStorage.setItem("language", selectedLanguage);
                sessionStorage.setItem("beerArray", JSON.stringify(beerArray));
                sessionStorage.setItem("credits", 1000);
                //console.log(beerArray);
                window.location.href ="index.html";
            }, 500);


        // print welcome message and redirect
        //window.location.href ="index.html";
    }
    else {
        // wrong psw for the user
        console.log("wrong password");
        $("#inputPassword").val('');
        $("#inputPassword").focus();
        $("#wrongPSW").html("<p>Wrong username or password</p>").delay(100).fadeIn(300);
        $("#wrongPSW").delay(800).fadeOut(200);
    }
} else {
    // both username and password was wrong
    console.log("wrong username or psw");
    $("#inputPassword").val('');
    $("#inputPassword").focus();
    $("#wrongPSW").html("<p>Wrong username or password</p>").delay(100).fadeIn(300);
    $("#wrongPSW").delay(800).fadeOut(200);
}
  event.preventDefault();
});
