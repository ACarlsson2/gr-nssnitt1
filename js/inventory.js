/*
*   Global variables
*/
var user = sessionStorage.getItem("userName");
var selectedLanguage = sessionStorage.getItem("language");
$("#user-text").html(user);

var beerArray = JSON.parse(sessionStorage.getItem("beerArray"));
var cartArray = [];
var redoArray = [];
var clearArray = [];

/*
*   The translation dictionary.
*   To be used as key-value
*/
var translation = {
    "en": {
        "order": "Order",
        "about": "About",
        "inventory": "Inventory",
        "signOut": "Sign Out",
        "allBeer": "All the Drinks",
        "search": "Search drinks",
        "howMany": "How many would you like to order?",
        "order": "Order",
        "close": "Close",
    },
    "sv": {
        "order": "Beställ",
        "about": "Om oss",
        "inventory": "Lager",
        "signOut": "Logga ut",
        "allBeer": "All dricka",
        "search": "Sök dricka",
        "howMany": "Hur många vill du beställa?",
        "order": "Beställ",
        "close": "Stäng",



    }
}

/*
*   function: language(language)
*   input: laguage to translate to
*   use: translate the page to the desired language by changing the HTML
*/
function translate(language) {
    selectedLanguage = language;
    sessionStorage.setItem("language", selectedLanguage);
  $(".multi-lang").each(function() {
      var tkey = $(this).attr("tkey");
    $(this).html(translation[selectedLanguage][tkey])
  })
};



// on page load
$(function() {
    loadBeer();
    translate(selectedLanguage);
});

/*
*   function: loadBeer()
*   input: none
*   use: sends a request to the API for information on all the drinks
*/
function loadBeer(){
    console.log("user = " + user);

    beerArray.forEach(function(ele) {
        var beerContainer = $("#beerContainer");
        var beer = $('<div class="beer" id="'+ele.beer.id+'" draggable="true" ondragstart="drag(event)" onclick=beerClick(event) ></div>');
        beerContainer.append(beer);
        //console.log(ele.beer.id);


        beer.append('<div class="col-lg-8"><div class="beerName">'+ele.beer.name+'</div></div>');
        beer.append('<div class="col-lg-3"><div class="beerPrice">' + ele.beer.orderPrice+ "kr" + '</div></div>');
        beer.append('<div class="col-lg-8"><div class="beerName2">' + ele.beer.name2 + '</div></div>');
        beer.append('<div class="col-lg-3"><div class="beerCount">' + ele.beer.count + '</div></div>');


    });
}

/*
*   function: beerClick(ev)
*   input: an event
*   use: when the user click on a drink a modal is opend
*/
function beerClick(ev) {
    var $beer = $(ev.target).closest(".beer");
    console.log($beer.find(".beerName").text());
    $(".modal-title").html($beer.find(".beerName").text());
    $(".modal-subtitle").html($beer.find(".beerName2").text());
    $('#myModal').modal("toggle");
}

/*
*   function: orderBeer()
*   input: none
*   use: takes the amount value from the modal and updates the invenory on that drink
*/
function orderBeer() {
    var amount = $("#order-amount").val();
    var name = $(".modal-title").text();
    var name2 = $(".modal-subtitle").text();
    $("#order-amount").val("");
    updateBeerArray(name, name2, amount);
}

/*
*   function: updateBeerArray(name, name2, amount)
*   input: name - first name on a drink. name2 - second name on a drink. amount - the amount a drink is to be changed with
*   use: Checks so that name and name2 matches a drink then updates the amount
*/
function updateBeerArray(name, name2, amount) {
    for (var i = 0; i < beerArray.length; i++) {
        if (beerArray[i].beer.name == name && beerArray[i].beer.name2 == name2) {
            console.log("updating " + name);
            beerArray[i].beer.count = parseInt(beerArray[i].beer.count) + parseInt(amount);

        }
    }
    saveBeer();
    $("#beerContainer").empty();
    loadBeer();
}


/*
*   handles the searchbar
*   on a keyup the string inside the searchbar is used to filter out drinks.
*/
$("#search").keyup(function() {
    //console.log("change");
    $(".beer").each(function() {
        var search = $("#search").val().toLowerCase();
        //console.log($(this).find(".beerName").text());
        if (search != "") {
            if ($(this).find(".beerName").text().toLowerCase().indexOf(search) < 0) {
                //console.log($(this).find(".beerName").text().toLowerCase());
                $(this).hide();
            }
            else {
                $(this).show();
            }
        }
        else $(this).show();


    });
});

/*
*   function: saveBeer()
*   input: none
*   use: save the array that contains all the drinks in sessionStorage
*/
function saveBeer() {
    sessionStorage.setItem("beerArray", JSON.stringify(beerArray));
}
